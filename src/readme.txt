=== Easy-Arrows ===
Contributors: Christoffer Helgelin Hald
Stable tag: 1.0
Tested up to: 5.0
Requires at least: 3.0

Super easy hyperlink SVG arrows. Use [arrow-right] and [arrow-left]. Thats all!

== Description ==

Super easy hyperlink SVG arrows. Use [arrow-right] and [arrow-left]. Thats all!