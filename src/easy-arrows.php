<?php
   /*!
   Plugin Name: Easy Arrows
   Plugin URI: https://christofferhald.dk/easy-arrows
   description: Super easy hyperlink SVG arrows. Pure CSS, no JavaScript. Use [arrow-right] and [arrow-left]. Thats it!
<<<<<<< HEAD
   Version: 0.4.0
=======
   Version: 0.2.0
>>>>>>> 619430d23f20cc476ee71a63940d5b7ea742fd5f
   Author: Christoffer Helgelin Hald
   Author URI: https://christofferhald.dk
   License: GPL2
   */


// [arrow-right]
function arrow_right_function(){
	$output = '<span class="easyarrows easyarrows--right">';
	$output .= '<svg version="1.1" class="easyarrows__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 50" style="enable-background:new 0 0 100 50;" xml:space="preserve"><rect x="0" y="24" width="64" height="6"/>
<polygon points="100,27 73,44 73,10 "/></svg>';
	$output .= '</span>';
	return $output;
}
add_shortcode('arrow-right','arrow_right_function');



// [arrow-left]
function arrow_left_function(){
	$output = '<span class="easyarrows easyarrows--left">';
	$output .= '<svg version="1.1" class="easyarrows__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 50" style="enable-background:new 0 0 100 50;" xml:space="preserve"><rect x="36" y="24" width="64" height="6"/>
<polygon points="0,27 27,10 27,44 "/></svg>';
	$output .= '</span>';
	return $output;
}
add_shortcode('arrow-left','arrow_left_function');




// Enqueues stylesheet
add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
<<<<<<< HEAD
	wp_register_style('easy-arrows', plugins_url('/easy-arrows.css', __FILE__), false, '0.4.0', 'all');
=======
	wp_register_style('easy-arrows', plugins_url('/easy-arrows.css', __FILE__), false, '0.2.0', 'all');
>>>>>>> 619430d23f20cc476ee71a63940d5b7ea742fd5f
	wp_enqueue_style( 'easy-arrows' );
}
?>