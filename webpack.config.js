const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== 'production';
const CleanWebpackPlugin = require('clean-webpack-plugin');




module.exports = {
    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: 'main.js'
    },

    resolve: {
        alias: {
            'images': path.resolve(__dirname, 'src/images/')
        }
    },

    module: {
<<<<<<< HEAD
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: './'
                        }
                    },
                    {   
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ],
=======
        rules: [{
            test: /\.(sa|sc|c)ss$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        publicPath: './'
                    }
                },
                {   
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    loader: 'sass-loader'
                }
            ],
>>>>>>> 619430d23f20cc476ee71a63940d5b7ea742fd5f
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: devMode ? 'easy-arrows.css' : 'easy-arrows.[hash].css'            
        }),
        
        new CleanWebpackPlugin(['dist'])
    ]

}